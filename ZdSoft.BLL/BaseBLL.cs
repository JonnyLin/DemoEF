﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace ZdSoft.BLL
{
    public class BaseBLL<T> 
        where T:class,new()
        //where M:DAL.BaseDal<T>,new()
    {
        static DAL.BaseDal<T> dal = new DAL.BaseDal<T>();
        public  IQueryable<TSelector> GetList<TSelector,TKey>(Expression<Func<T,bool>> whereLambda,Expression<Func<T,TSelector>> selector,Expression<Func<T,TKey>> orderLambda,bool isAsc=true)
        {
            return dal.GetList(whereLambda, selector, orderLambda,isAsc);
        }

    }
}
