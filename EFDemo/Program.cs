﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZdSoft.Model;
using ZdSoft.BLL;

namespace EFDemo
{
    class Program
    {
        static void Main(string[] args)
        {
           var list = new BaseBLL<SystemUser>().GetList((model) => model.IsActive == true, (mode) =>mode.Account+","+mode.UserName+","+mode.IsActive, (mode) => mode.Account+","+mode.UserName).ToList();
           foreach (var item in list)
               Console.WriteLine(item +"\n");
           Console.ReadLine();
        }
    }
}
