﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using System.Linq.Expressions;

namespace ZdSoft.DAL
{
    public class BaseDal<T> where T:class,new()
    {
        static Model.OnlineStudyEntities db = new Model.OnlineStudyEntities();
        /// <summary>
        /// 根据条件获取数据集合
        /// </summary>
        /// <typeparam name="TSelector">返回特定属性</typeparam>
        /// <typeparam name="TKey">选择的属性</typeparam>
        /// <param name="whereLambda">条件</param>
        /// <param name="selector"></param>
        /// <param name="orderLambda">排序</param>
        /// <param name="isAsc">顺序排序，默认为true</param>
        /// <returns></returns>
        public IQueryable<TSelector> GetList<TSelector,TKey>(Expression<Func<T,bool>> whereLambda,Expression<Func<T,TSelector>> selector,Expression<Func<T,TKey>> orderLambda,bool isAsc=true)
        {
            if(isAsc)
            {
                return db.Set<T>().Where(whereLambda).OrderBy(orderLambda).AsNoTracking().Select(selector);
            }
            else
            {
                return db.Set<T>().Where(whereLambda).OrderByDescending(orderLambda).AsNoTracking().Select(selector);
            }
        }

    }
}
